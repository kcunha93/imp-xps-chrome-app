module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        exec: {
            npm: {
                cwd: '',
                cmd: 'npm install'
            },

            zip: {
                cwd: '',
                cmd: 'zip -r -y _DeveloperDashboard.zip app'
            }
        },

        webpack: {
            options : {
                stats : {
                    colors  : true,
                    modules : true,
                    reasons : true
                },

                progress    : true,
                failOnError : true,
                devTool     : 'source-map',

                keepAlive : false,
                watch     : false,

                module : {
                    loaders : [
                        {
                            test    : /\.jsx?$/,
                            exclude : /node_modules/,
                            loader  : 'babel?presets[]=react,presets[]=es2015'
                        }
                    ]
                }
            },

            build: {
                entry: {
                    index: './app/js/index.js'
                },

                output: {
                    path: './app/js/',
                    filename: '[name]-bundle.js'
                }
            }
        },
        uglify: {
            options: {
                mangle: {
                    topLevel: true,
                    eval: true
                },
                compress: {
                    dead_code: true,
                    unused: true,
                    drop_console: true
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-webpack');
    //grunt.loadNpmTasks('grunt-string-replace');

    // Default task(s).
    grunt.registerTask('default', [
        'webpack',
        'uglify',
        'exec:zip'
    ]);
};