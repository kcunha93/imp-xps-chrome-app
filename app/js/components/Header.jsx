const React = require('react');

module.exports = function ({actions}){
  return React.createClass({
    displayName: 'Header',
    handleClear: function (){
      actions.clearQueries();
    },
    render: function (){
      return (
        <div className="header-container">
          <h1>XPS Query Monitor</h1>
          <button id={'panel-clear'} onClick={this.handleClear}>
            CLEAR QUERIES 
          </button>
        </div>
      );
    }
  });
};