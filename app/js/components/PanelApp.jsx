const React = require('react');
const createHeader = require('./Header.jsx');
const createQueryList = require('./QueryList.jsx');

module.exports = function (deps){
  const Header = createHeader(deps);
  const QueryList = createQueryList(deps);
  return React.createClass({
    displayName: 'PanelApp',
    render: function (){
      return (
        <div className="panel-container">
          <Header />
          <QueryList />
        </div>
      );
    }
  });
};