const React = require('react');
const _ = require('lodash');
import JSONTree from 'react-json-tree';

module.exports = function (actions){
  return React.createClass({
    displayName: 'Query',
    propTypes: {
      queryData: React.PropTypes.object
    },
    handleExplain: function (){
      actions.explainQuery({url: this.props.queryData.url});
    },
    handleLemm: function (){
      let lemmText = _.find(this.props.queryData.queryParams, (tmp) => {
        return tmp.name === 'q';
      });
      actions.lemmatizeQuery({queryText: lemmText.value});
    },
    render: function (){
      let queryText, query, status, params, xpsEnv, custId, custLi,
        envLi, responseJSON;
      queryText = _.find(this.props.queryData.queryParams, (tmp) => {
        return tmp.name === 'q';
      });
      query = (queryText) 
        ? (
          <div>
            <span 
              className="query-text"
            >
              {'Query Text: ' + decodeURIComponent(queryText.value)}
            </span>
            <button className="lemm-button" onClick={this.handleLemm}>
              Lemmatize this
            </button>
          </div>)
        : '';
      status = 'Status Code: ' + this.props.queryData.status;
      params = this.props.queryData.queryParams.map(function(param,i) {
        let string;
        if(param.name === 'relevantFacts' || param.name === 'removedFacts'
        || param.name === 'weather'){
          string = param.name + ' :';
          return(
            <li className={'query-params'} key={i}>
              {string}
              <JSONTree
                data={JSON.parse(
                  decodeURIComponent(decodeURIComponent(param.value))
                )}
                hideRoot={true}
              />
            </li>
          );
        } else {
          string = param.name + ' : ' + param.value;
          return (
            <li className={'query-params'} key={i}>
              {string}
            </li>
          );
        }
      });
      xpsEnv = 'xpsEnv : ' + 
        this.props.queryData.url.split('.')[0].split('//')[1];
      custId = 'custId : ' + this.props.queryData.url.split('/')[4];
      custLi = (
        <li className={'query-params'}>
          {custId}
        </li>
      );
      envLi = (
        <li className={'query-params'}>
          {xpsEnv}
        </li>
      );
      responseJSON = JSON.parse(this.props.queryData.response);

      return (
        <div className="query-container">
          {query}
          <br/>
          <span className={'query-status'}>{status}</span>
          <ul>
            <li className={'params-title'}>Query Parameters</li>
            {envLi}
            {custLi}
            {params}
          </ul>
          <span className={'response-title'}>Response:</span>
          <div className={'json-wrapper'}>
            <JSONTree data={responseJSON} hideRoot={true} />
          </div>
          <button 
            className={'explain-button'}
            onClick={this.handleExplain}
          >
            Explain Response
          </button>
          <hr/>
        </div>
      );
    }
  });
};