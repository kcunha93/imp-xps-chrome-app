const React = require('react');
const createQuery = require('./Query.jsx');

module.exports = function ({store, actions}){
  const Query = createQuery(actions);
  return React.createClass({
    displayName: 'QueryList',
    getInitialState: function (){
      return {
        queries: store.getResponses()
      };
    },
    componentDidMount: function (){
      store.subscribe(this.handleChange);
    },
    handleChange: function (){
      this.setState({
        queries: store.getResponses()
      });
    },
    render: function (){
      let queries = this.state.queries.map(function(query, i) {
        return(
          <Query
            queryData={query}
            key={i}
          />
        );
      });
      return (
        <div className="query-list-container">
          <span className="query-list-title">Queries:</span>
          <hr/>
          {queries}
        </div>
      );
    }
  });
};