var storeBase = require('../lib/storeBase');

module.exports = function (dispatcher){
  var store = Object.create(storeBase.prototype);

  var responses = [];

  store.getResponses = function(){
    return responses;
  }

  dispatcher.register('store', function (action){

    switch (action.type) {
      case 'recordResponse':
        responses.push({
          'url': action.url,
          'queryParams': action.queryParams,
          'response': action.responseData,
          'status' : action.status
        });
        store.emitChange();
        break;
      case 'clearQueries':
        responses = [];
        store.emitChange();
        break;
      case 'explainQuery':
        window.open(action.url.replace(
          'productResourceContent',
          'explain~productResourceContent'), 
        '_blank');
        break;
      case 'lemmatizeQuery':
        let lemmUrl = 'https://xps.fluidretail.net/lemmatize/?q=' +
          action.queryText;
        window.open(lemmUrl, '_blank');
        break;
      default:
    }
  });

  return store;
};