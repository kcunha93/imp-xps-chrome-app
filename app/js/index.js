const React = require('react');
const ReactDOM = require('react-dom');
const createActions = require('./actions');
const dispatcher = require('./lib/dispatcher')();
const actions = require('./lib/mapActions')(createActions, dispatcher);
const store = require('./stores/store')(dispatcher);
const PanelApp = require('./components/PanelApp.jsx')({store, actions});

window.receiveData = function(response){
  actions.recordResponse(response);
}

ReactDOM.render(<PanelApp />, document.getElementById('content'));