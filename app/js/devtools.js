chrome.devtools.panels.create('XPS','XPS.png','panel.html',function(panel) {
  var _window;

  var data = [];
  chrome.devtools.network.onRequestFinished.addListener(function(request) {
    if(request.request.url.indexOf('xps.fluidretail.net/customers/') !== -1) {
      var response = request.getContent(function(body){
        var responseBody = {
          'url':request.request.url,
          'status': request.response.status,
          'response':body,
          'params': request.request.queryString
        };
        if(_window){
          _window.receiveData(responseBody);
        } else {
          data.push(responseBody);
        }
      });
    }
  });

  panel.onShown.addListener(function tmp(panelWindow) {
    panel.onShown.removeListener(tmp);
    _window = panelWindow;
    // Release queued data
    var response;
    while (response = data.shift()) 
        _window.receiveData(response);
  });
});
