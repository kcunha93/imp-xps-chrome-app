module.exports = function (dispatcher){
  return function (action){
    dispatcher.dispatch({
      type: 'explainQuery',
      url: action.url
    });
  };
};