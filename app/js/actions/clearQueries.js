module.exports = function (dispatcher){
  return function (action){
    dispatcher.dispatch({
      type: 'clearQueries'
    });
  };
};