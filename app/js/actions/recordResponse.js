module.exports = function (dispatcher){
  return function (action){
    dispatcher.dispatch({
      type: 'recordResponse',
      responseData: action.response,
      url: action.url,
      queryParams: action.params,
      status: action.status
    });
  };
};