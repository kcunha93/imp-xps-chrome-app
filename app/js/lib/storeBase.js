var events = require('events');
var util = require('util');

function storeBase (){
  events.EventEmitter.call(this);
}

util.inherits(storeBase, events.EventEmitter);

storeBase.prototype.subscribe = function (callback){
  this.on('change', callback);
};

storeBase.prototype.unsubscribe = function (callback){
  this.removeListener('change', callback);
};

storeBase.prototype.emitChange = function (){
  this.emit('change');
};

module.exports = storeBase;