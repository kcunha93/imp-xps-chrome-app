var _ = require('lodash');

module.exports = function (collection, dispatcher){
  return _.chain(collection)
    .map(function (action, name) {
      return {
        name: name,
        action: action(dispatcher)
      };
    }).reduce(function (result, n){
      result[n.name] = n.action;
      return result;
    }, {})
    .value();
}
