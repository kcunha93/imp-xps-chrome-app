var _ = require('lodash');

module.exports = function createDispatcher (){
  var callbacks = [];
  var dispatching = false;

  var dispatcher = _.assign({}, require('events').EventEmitter.prototype);
  // Store the cb to run during dispatches
  dispatcher.register = function (name, cb){
    callbacks.push({
      name: name,
      fn: cb
    });
    return name;
  };
  // Run the stored callbacks
  dispatcher.dispatch = function (action, callback){
    if (callbacks.length === 0){
      throw new Error('No callbacks are registered!');
    }
    if (!dispatching){
      // Prevent race conditions by preventing multiple
      // dispatches from being run at once.
      dispatching = true;

      for (let i = 0; i < callbacks.length; i += 1){
        let cb = callbacks[i];
        cb.fn(action);
        // Emit the event so the waitFor method knows it can continue
        this.emit('callback:completed', cb.name);
      }

    }else {
      throw new Error('Cannot call dispatch during a running dispatch');
    }
    // Dispatch is over
    dispatching = false;
    if (callback) { callback(); }
  };
  // Wait for a list of callbacks to run
  dispatcher.waitFor = function (names, callback){
    this.on('callback:completed', (cbName) =>{
      names = _.filter(names, cbName);
      if (names.length === 0) { callback(); }
    });
  };

  return dispatcher;
};
